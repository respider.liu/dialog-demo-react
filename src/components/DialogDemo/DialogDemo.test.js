import 'babel-polyfill';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});

import DialogDemo from './index.js';

describe('DialogDemo', () => {
  const wrapper = mount(<DialogDemo />);
  it('popup a dialog that includes a table', async () => {
    const openBtn = wrapper.find('.ant-btn-primary').at(0) // 拿到触发弹窗显示的按钮

    expect(wrapper.state('visible')).toBeFalsy() // 控制弹窗的变量初始值为false
    await openBtn.simulate('click') // 触发按钮的单击事件

    expect(wrapper.state('visible')).toBeTruthy() // 控制弹窗的变量值是否变为true
  });

  it('show 30+ recodes in the table', async () => {
    const rows = wrapper.find('tr.ant-table-row'); // 拿到所有的行
    expect(rows.length).toEqual(30);
  });

  it('hide the dialog', async () => {
    // const dialog = wrapper.find('#dialog') // 拿到弹窗
    const closeBtn = wrapper.find('.ant-modal-close') // 拿到触发弹窗显示的按钮

    expect(wrapper.state('visible')).toBeTruthy() // 控制弹窗的变量初始值为true
    await closeBtn.simulate('click') // 触发按钮的单击事件
    expect(wrapper.state('visible')).toBeFalsy() // 控制弹窗的变量值是否变为false
  });

});
