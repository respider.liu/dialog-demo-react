import React from 'react';
// import ReactDOM from 'react-dom';
import { Modal, Button, Table } from 'antd';
import './index.less';
// import App from "../App";

class DialogDemo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      currentPage: 1,
      pageSize: 30,
      total: 0,
      dataSource: [],
      tableData: []
    };
  }

  componentDidMount() {
    // 循环生成100条数据
    for (let i = 0; i < 100; i++) {
      this.state.tableData.push(
          {
            id: i,
            name: '王小虎',
            address: `上海市普陀区金沙江路 ${100 + i} 弄`,
            key: `row-${i}`
          }
      )
    }
    this.setState({total: this.state.tableData.length});
    this.setDataSource();
  }

  setDataSource = () => {
    let start = (this.state.currentPage - 1) * 30; // 页码从1开始，截取数据从0开始，所以要减一
    const newArr = this.state.tableData.slice(start, start + 30);
    this.setState({dataSource: newArr});
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  // 翻页按钮事件
  handlePagin(p, ps) {
    this.setState({currentPage: p}, () => {
      this.setDataSource();
    });
  }

  render() {
    const winWidth = document.body.clientWidth;
    const winHeight = document.body.clientHeight;
    const paginationProps = {
      size: 'small',
      showSizeChanger: false,
      showQuickJumper: false,
      showTotal: () => `共${this.state.total}条`,
      pageSize: this.state.pageSize,
      current: this.state.currentPage,
      total: this.state.total,
      onChange: (current) => this.handlePagin(current),
    }

    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '地址',
        dataIndex: 'address',
        key: 'address',
      },
    ];
    return (
        <div className="wrapper">
          <Button type="primary" onClick={this.showModal}>打开嵌套表格的 Dialog</Button>
          <Modal
              title="收货地址"
              visible={this.state.visible}
              width={Math.max(winWidth, winHeight) / 2}
              footer={[
                <Button key="submit" type="primary" onClick={this.handleOk}>
                  确定
                </Button>,
              ]}
              onCancel={this.handleCancel}
          >
            <Table dataSource={this.state.dataSource} columns={columns} pagination={paginationProps} className="dialog-table" scroll={{ y: 300 }} />
          </Modal>
      </div>
    );
  }
}

export default DialogDemo;
