import React, { Component} from "react";
import "./App.less";
import "antd/dist/antd.css"
import DialogDemo from "./components/DialogDemo";

class App extends Component{
  render(){
    return(
      <div className="App">
        <DialogDemo />
      </div>
    );
  }
}

export default App;
