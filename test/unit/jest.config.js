const path = require("path");

module.exports = {
  rootDir: path.resolve(__dirname, '../../'), // 类似 webpack.context
  moduleFileExtensions: ['js', 'jsx'],
  testPathIgnorePatterns: ['/node_modules/'],
  testRegex: '.*\\.test\\.js$',
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1', // 类似 webpack.resolve.alias
    '\\.(css|less)$': '<rootDir>/test/unit/styleMock.js',
  },
  // setupFiles: ['<rootDir>/test/unit/setup.js'], // 类似 webpack.entry
  coverageDirectory: '<rootDir>/test/unit/coverage', // 类似 webpack.output
  collectCoverageFrom: [ // 类似 webpack 的 rule.include
    'src/components/**/*.{js}',
    '!src/index.js',
    '!src/App.js',
    '!**/node_modules/**',
  ],
};
