# dialog-demo-react

#### 实现一个弹出框，从页面点击一个按钮后弹出这个弹框。弹框中需要同时具备以下特征：
*  具有关闭和确认按钮；
*  弹框宽度为视窗宽度和高度两者最大值的一半, 高度不大于500px，水平垂直居中；
*  弹框中间展示一个固定头部的表格, 表格中每页上下滚动展示30条行高32px的数据，具有分页效果
*  对视窗的“打开、关闭、展示30条数据”三条逻辑添加test cases。


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn unit
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

> 因为 React 官方的 create-react-app 工具仅能创建一个基本的React开发环境，不包含webpack和babel等工具，所以并不适用于创建复杂的应用。虽然使用 ejecting 可以“弹出”相关的配置项，但是冗余代码过多，不符合精简的要求，所以我们可以手动创建一个React项目。

### 1. 项目初始化
##### 首先在终端中创建项目目录，然后在该目录中执行 `npm init` 并按照提示初始化npm配置。
~~~ bash
➜  mkdir myApp
➜  cd myApp
➜  npm init
~~~

##### 如果需要用git管理代码，同时再执行 `git init` 初始化git仓库。
~~~ bash
➜  git init
~~~
##### 并新建 `.gitignore` 文件，设置不想提交到git的忽略项。
~~~ gitignore
.DS_Store
node_modules/
/dist/
coverage/
npm-debug.log*
yarn-debug.log*
yarn-error.log*
package-lock.json
yarn.lock


# Editor directories and files
.idea
.history
.vscode
*.suo
*.ntvs*
*.njsproj
*.sln
~~~

##### 在项目目录中分别新建 `public` 和 `src` 目录；
~~~ bash
➜  mkdir public
➜  mkdir src
~~~

##### `public` 目录用于部署所有的静态资源（静态资源最好能部署到CDN），其中最重要的是 `index.html`
~~~ html
<!-- sourced from https://raw.githubusercontent.com/reactjs/reactjs.org/master/static/html/single-file-example.html -->
<!DOCTYPE html>
<html lang="zh">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>React Starter</title>
</head>

<body>
  <!-- React app 根容器 -->
  <div id="root"></div>
  <noscript>
    You need to enable JavaScript to run this app.
  </noscript>
  <!-- Webpack 构建的运行时js，所有的React组件和业务逻辑都会被打包到这个js文件里 -->
  <script src="../dist/bundle.js"></script>
</body>

</html>
~~~

##### 接下来，我们需要确保编写的代码能够被正常编译，所以我们需要 [Babel](https://babeljs.io/)
~~~ bash
➜  yarn add -D @babel/core@7.1.0 @babel/cli@7.1.0 @babel/preset-env@7.1.0 @babel/preset-react@7.0.0 babel-polyfill
~~~
> babel-core 是babel的核心；babel-cli是命令行工具；preset-env和preset-react是babel的代码风格预设，表示可以转换es6+的js语法和React特有的JSX语法。babel-polyfill使得nodejs环境可以解析es6语法。

##### 新建描述文件 `.babelrc`，并做如下配置
~~~ js
{ 
  "presets": ["@babel/env", "@babel/preset-react"] 
}
~~~ 

##### 现在，我们需要构建项目，并且让它正常启动运行，我们需要配置 `Webpack` 
~~~ bash
➜  yarn add -D webpack@4.19.1 webpack-cli@3.1.1 webpack-dev-server@3.1.8 style-loader@0.23.0 css-loader@1.0.0 babel-loader@8.0.2
~~~
> webpack是核心，webpack-cli是命令行工具，webpack-dev-server可以启动一个开发环境的nodejs服务；style-loader，css-loader，babel-loader等各种loader可以在构建项目时处理不同类型的文件（如将预处理CSS语法转换为标准CSS语法等）。

##### 新建一个配置文件 webpack.config.js ，并做如下配置
~~~ js
const path = require("path");
const webpack = require("webpack");

module.exports = {
  // build 入口
  entry: "./src/index.js",
  // 标记为开发模式，可以启动一个nodejs服务
  mode: "development",
  // 定义loader的转换规则
  module: {
    rules: [
      // 转换js中的es6语法和React的jsx语法
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] }
      },
      // css-loader使你能够使用类似@import和url（…）的方法实现require的功能，style-loader将所有的计算后的样式加入页面中，
      // 二者组合在一起使你能够把样式表嵌入webpack打包后的js文件中
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      // 我们可能需要使用less或sass等预处理语法方便开发，也需要安装并配置对应的loader
      {
        test: /\.less$/,
        use: ['style-loader','css-loader','less-loader']
      }
    ]
  },
  // 定义需要转换的文件类型
  resolve: { extensions: ["*", ".js", ".jsx", ".css", ".less"] },
  // 定义构建后输出的文件及路径
  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/dist/",
    filename: "bundle.js"
  },
  // 开发模式nodejs服务的配置
  devServer: {
    contentBase: path.join(__dirname, "public/"),
    port: 3000,
    publicPath: "http://localhost:3000/dist/",
    hotOnly: true
  },
  // 插件
  plugins: [
    // 开发模式热更新插件
    new webpack.HotModuleReplacementPlugin()
  ]
};
~~~

### 2. 开始编写React代码
~~~ bash
# 编写此文档时react的版本是17.0.1，可根据实际情况安装指定版本
➜  yarn add react react-dom
# 安装react-hot-loader
➜  yarn add -D react-hot-loader
# 如果需要用到预处理css，需要安装less或sass
➜  yarn add less less-loader
~~~
~~~ jsx
// 在src目录中新建 index.js
 
import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

ReactDOM.render(<App />, document.getElementById("root"));
~~~
~~~ jsx
// 在src目录中新建另一个文件 App.js 
// 编写一个 React 组件的基本代码，其中包含了支持 webpack-dev-server 热加载的组件

import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";

class App extends Component{
  render(){
    return(
      <div className="App">
        <h1> Hello, World! </h1>
      </div>
    );
  }
}

export default hot(module)(App);
~~~
~~~ css
/* 再新建一个CSS文件 App.css, 为App编写一个简单的样式 */
.App { 
  margin: 1rem; 
  font-family: Arial, Helvetica, sans-serif; 
}
~~~
##### 以下就是完整的项目结构
~~~
.
+-- public
| +-- index.html
+-- src
| +-- App.css
| +-- App.js
| +-- index.js
+-- .babelrc
+-- .gitignore
+-- package.json
+-- webpack.config.js
~~~

##### 在 package.json 文件的 scripts 里加入 webpack-dev-server 的启动命令
~~~ json
"scripts": {
  "start": "webpack-dev-server --mode development"
}
~~~

##### 然后执行安装依赖和启动命令
~~~ bash
➜  yarn install
➜  yarn start
~~~

### 3. 添加单元测试
##### 安装依赖 `jest` 等，并编写相关配置 
~~~ bash
➜  yarn add -D jest react-test-renderer enzyme @wojtekmaj/enzyme-adapter-react-17
# 这里使用react 17，目前enzyme官方还没有兼容的adapter，所以使用非官方的@wojtekmaj/enzyme-adapter-react-17
➜  mkdir -p test/unit
➜  cd test/unit
➜  vi jest.config.js
~~~
~~~ js
const path = require("path");

module.exports = {
  rootDir: path.resolve(__dirname, '../../'), // 类似 webpack.context
  moduleFileExtensions: ['js', 'jsx'],
  testPathIgnorePatterns: ['/node_modules/'],
  testRegex: '.*\\.test\\.js$',
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1', // 类似 webpack.resolve.alias
    '\\.(css|less)$': '<rootDir>/test/unit/styleMock.js',
  },
  // setupFiles: ['<rootDir>/test/unit/setup.js'], // 类似 webpack.entry
  coverageDirectory: '<rootDir>/test/unit/coverage', // 类似 webpack.output
  collectCoverageFrom: [ // 类似 webpack 的 rule.include
    'src/components/**/*.{js}',
    '!src/index.js',
    '!src/App.js',
    '!**/node_modules/**',
  ],
};
~~~
##### 新建一个文件，导出一个空对象，用来 mock jsx 中 import css，避免测试时报错
~~~ bash
touch styleMock.js
vi styleMock.js
~~~
~~~ js
module.exports = {};
~~~
##### 写一个单元测试
~~~ bash
# 在src目录里新建一个文件 App.test.js
touch App.test.js
vi App.test.js
~~~
~~~ js
import 'babel-polyfill';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-17';
configure({adapter: new Adapter()});

import App from './App.js';

describe('App', () => {
  const wrapper = mount(<App />);
  it('display a title "Hello, World!"', async () => {

    expect(wrapper.find('h1').text()).toEqual('Hello, World!') // 控制弹窗的变量值是否变为true
  });
});
~~~
